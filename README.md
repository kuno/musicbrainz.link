
musicbrainz.link
================

This projects attempts to build a completed Linked Data dump of MusicBrainz, based
on the JSON-LD snippets embedded in each entity page.

For now, the focus is on artists.  Other entities (releases, recordings, etc..) will
be considered in the future.

Setup
-----

Run bin/bootstrap


Temporary instructions
----------------------

This project is not complete and needs a bunch of automation and error handling.

What exists so far is prototype scripts to download the JSON-LD for an artist and inject that
into fuseki.

If no fuseki dataset has been created yet, create that first:

    bin/create-dataset

Then try to download and process Britney Spears:

    bin/fetch https://musicbrainz.org/artist/45a663b5-b1cb-4a91-bff6-2bef7bbfdd76
    bin/process-download tmp/*.json


Commands
--------

    bin/bootstrap                # install dependencies
    bin/create-dataset           # create the fuseki dataset
    bin/datastore                # start/stop fuseki
    bin/test                     # run tests

    bin/run                      # download and process URLs (runs continuously)
    - bin/download-next-url      # gets the next url and starts processing it
      - bin/process-url          # mark url as being downloaded, and start downloading
        - bin/fetch              # download url, extract jsonld, save to .jsonld file
        - bin/process-download   # process downloaded .jsonld files
          - bin/jsonld-to-nt     # convert .jsonld file into .nt file

License
=======

Copyright 2016  Kuno Woudt <kuno@frob.nl>

This program is free software: you can redistribute it and/or modify
it under the terms of copyleft-next 0.3.1.  See
[copyleft-next-0.3.1.txt](copyleft-next-0.3.1.txt).

