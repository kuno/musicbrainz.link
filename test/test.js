/**
 *   This file is part of musicbrainz.link.
 *   Copyright (C) 2016  Kuno Woudt <kuno@frob.nl>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of copyleft-next 0.3.1.  See copyleft-next-0.3.1.txt.
 */

'use strict';

const entity = require ('../lib/entity');
const fs = require ('fs');
const should = require ('chai').should;

should ();

const 윤미래Id = 'https://musicbrainz.org/artist/d2faa4dd-f3fc-4aa9-aa9e-001826d6fdd2';
const savePath = __dirname + '/tmp/';
const lemonsId = 'https://musicbrainz.org/release-group/00014d9a-830d-3656-9b19-39bfd099b1a6';

function fetchPageMock (url) {
    return new Promise ((resolve, reject) => {
        switch (url) {
            case 'https://musicbrainz.org/artist/d2faa4dd-f3fc-4aa9-aa9e-001826d6fdd2':
            case 'https://musicbrainz.org/artist/d2faa4dd-f3fc-4aa9-aa9e-001826d6fdd2?page=1':
                resolve (fs.readFileSync ('test/data/yoon.mi-rae.html', 'UTF-8'));
                break;
            case 'https://musicbrainz.org/artist/d2faa4dd-f3fc-4aa9-aa9e-001826d6fdd2/details':
                resolve (fs.readFileSync ('test/data/yoon.mi-rae.details.html', 'UTF-8'));
                break;
            case 'https://musicbrainz.org/release-group/00014d9a-830d-3656-9b19-39bfd099b1a6':
                resolve (fs.readFileSync (
                    'test/data/melody.of.certain.damaged.lemons.html',
                    'UTF-8')
                );
                break;
            default:
                reject (404);
        }
    });
}

suite ('musicbrainz', function () {
    let result윤미래 = false;

    test ('parsePage', function () {
        return fetchPageMock (윤미래Id).then (body => {
            const page = entity.parsePage (body, 윤미래Id);

            page.should.be.a ('string');

            const data = JSON.parse (page);

            data.should.include.keys ('@id', '@context');
            data['@id'].should.equal (윤미래Id);
            data['@context'].should.equal ('http://schema.org');
        });
    });

    test ('parsePage (empty)', function () {
        return fetchPageMock (윤미래Id + '/details').then (body => {
            const page = entity.parsePage (body, 윤미래Id + '/details');

            page.should.be.a ('string');

            const data = JSON.parse (page);

            data.should.include.keys ('@id', '@context');
            data['@id'].should.equal (윤미래Id);
            data['@context'].should.equal ('http://schema.org');
        });
    });

    test ('parsePage (TypeError bug)', function () {
        return fetchPageMock (lemonsId).then (body => {
            const page = entity.parsePage (body, lemonsId);

            page.should.be.a ('string');

            const data = JSON.parse (page);

            data.should.include.keys ('@id', '@context');
            data['@id'].should.equal (lemonsId);
            data['@context'].should.equal ('http://schema.org');
        });
    });

    test ('fetchPage', function () {
        return entity.fetchPage (윤미래Id, fetchPageMock).then (results => {
            results.should.be.a ('string');

            const page = JSON.parse (results);

            page.should.include.keys ('@id', '@context');
            page['@id'].should.equal (윤미래Id);
            page['@context'].should.equal ('http://schema.org');

            result윤미래 = results;
        });
    });

    test ('parseMBID', function () {
        const result = entity.parseMBID (윤미래Id);

        result.should.have.keys ('entity', 'id');
        result.entity.should.equal ('artist');
        result.id.should.equal ('d2faa4dd-f3fc-4aa9-aa9e-001826d6fdd2');
    });

    test ('parseMBID (release-group)', function () {
        const result = entity.parseMBID (lemonsId);

        result.should.have.keys ('entity', 'id');
        result.entity.should.equal ('release-group');
        result.id.should.equal ('00014d9a-830d-3656-9b19-39bfd099b1a6');
    });

    test ('cleanFilename', function () {
        const base = 'https://musicbrainz.org/artist/0001cd84-2a11-4699-8d6b-0abf969c5f06';
        const withVa = base + '?va=1&amp;all=1';
        const videos = base + '/recordings?video=1';

        entity.cleanFilename (withVa).should.equal (
            'artist.0001cd84-2a11-4699-8d6b-0abf969c5f06.va.1.all.1'
        );

        entity.cleanFilename (videos).should.equal (
            'artist.0001cd84-2a11-4699-8d6b-0abf969c5f06.recordings.video.1'
        );
    });

    test ('savePage', function () {
        before (() => result윤미래);

        const filename = savePath + 'artist.d2faa4dd-f3fc-4aa9-aa9e-001826d6fdd2.jsonld';

        try {
            fs.unlinkSync (filename);
        } catch (e) {
            // if the file doesn't exist, that's OK.
        }

        return entity.savePage (savePath, 윤미래Id, result윤미래).then (() => {
            const dataStr = fs.readFileSync (filename, 'UTF-8');
            const page = JSON.parse (dataStr);

            page.should.include.keys ('@id', '@context');
            page['@id'].should.equal (윤미래Id);
            page['@context'].should.equal ('http://schema.org');
        });
    });
});
