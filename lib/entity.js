/**
 *   This file is part of musicbrainz.link.
 *   Copyright (C) 2016  Kuno Woudt <kuno@frob.nl>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of copyleft-next 0.3.1.  See copyleft-next-0.3.1.txt.
 */

'use strict';

const cheerio = require ('cheerio');
const fetch = require ('node-fetch');
const fs = require ('fs');

function owl (term) {
    return 'http://www.w3.org/2002/07/owl#' + term;
}

function cleanFilename (str) {
    return (str
        .toLowerCase ()
        .replace ('https://musicbrainz.org/', '')
        .replace ('&amp;', '&')
        .replace (/[^a-z0-9-]/g, '.')
    );
}

function parseMBID (url) {
    const uuid = '[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}';
    const MBIDregex = new RegExp ('musicbrainz.org/([a-z-]+)/(' + uuid +')');

    const matches = url.match (MBIDregex);
    if (!matches) {
        return null;
    }

    if (matches.length > 2) {
        return { entity: matches[1], id: matches[2] };
    } else {
        return null;
    }
}

function fetcher (url) {
    return new Promise ((resolve, reject) => {
        fetch (url).then (res => {
            if (res.ok) {
                resolve (res.text ());
            } else {
                reject (res);
            }
        }, err => {
            reject (err);
        });
    });
}

function parsePage (html, url) {
    const doc = cheerio.load (html);
    const jsonld = doc ('script[type="application/ld+json"]').text ();

    if (jsonld && jsonld !== '') {
        return jsonld;
    }

    // Page was downloaded correctly, but contains no data.  Let's construct a valid
    // but empty JSON-LD document so we don't have to worry about it in the rest of
    // the processing chain.

    const mbid = parseMBID (url);
    const data = {
        '@context': 'http://schema.org',
        '@id': 'https://musicbrainz.org/' + mbid.entity + '/' + mbid.id
    };

    return JSON.stringify (data, null, 4);
}

function fetchPage (url, fetcher) {
    return fetcher (url).then (body => {
        return parsePage (body, url);
    });
}

function savePage (path, url, page) {
    const parsed = JSON.parse (page);
    const mbid = parseMBID (parsed['@id']);

    if (!mbid) {
        return Promise.reject ('No @id found');
    }

    // MusicBrainz matches the scheme in the '@id' with how the page was loaded, so
    // the ID may be:
    //     - https://musicbrainz.org/artist/:mbid
    //     - http://musicbrainz.org/artist/:mbid
    // But they're obviously equivalent, so let's add that bit of information.

    const httpUrl = parsed['@id'].replace ('https://', 'http://');
    if (httpUrl !== parsed['@id']) {
        parsed[owl ('sameAs')] = { '@type': '@id', '@id': httpUrl };
    }

    return new Promise ((resolve, reject) => {
        // write the data to a tmp file first, so that creation of the full .json file
        // appears atomic, and the .json file can be safely processed as soon as it exists.
        const tmpName = path + cleanFilename (url) + '.tmp';
        const filename = path + cleanFilename (url) + '.jsonld';

        fs.writeFileSync (tmpName, JSON.stringify (parsed, null, 4));
        fs.renameSync (tmpName, filename);
        resolve (filename);
    });
}

module.exports = {
    cleanFilename: cleanFilename,
    fetcher: fetcher,
    fetchPage: fetchPage,
    parseMBID: parseMBID,
    parsePage: parsePage,
    savePage: savePage,
};
